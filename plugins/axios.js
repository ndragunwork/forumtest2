import axios from 'axios'
import swal from 'sweetalert2'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

export default ({ app, store, redirect }) => {
  axios.defaults.baseURL = process.env.apiUrl

  if (process.server) {
    return
  }

  // Request interceptor
  axios.interceptors.request.use(request => {
    request.baseURL = process.env.apiUrl

    const token = store.getters['auth/token']

    if (token) {
      request.headers.common['Authorization'] = `Bearer ${token}`
    }

    return request
  })

  // Response interceptor
  axios.interceptors.response.use(response => response, error => {
    const { status } = error.response || {}

    if (status >= 500) {
      swal({
        type: 'error',
        title: 'error_alert_title',
        text: 'error_alert_text',
        reverseButtons: true,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel'
      })
    }


    if (status === 401) {

      console.log('test');

      redirect({ name: 'home' })
    }

    if (status === 401 && store.getters['auth/check']) {

      swal({
        type: 'warning',
        title: 'token_expired_alert_title',
        text: 'token_expired_alert_text',
        reverseButtons: true,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel'
      }).then(async () => {
        await store.dispatch('auth/logout')

        redirect({ name: 'welcome' })
      })
    }

    return Promise.reject(error)
  })
}
