import axios from 'axios'
import Cookies from 'js-cookie'

export const state = () => ({
  user: null,
  token: null,
  authState: null,
})

export const getters = {
  user: state => state.user,
  token: state => state.token,
  authState: state => state.authState,
  check: state => state.user !== null
}

export const mutations = {

  SET_TOKEN (state, token) {
    state.token = token
  },

  SET_AUTH_STATE(state, getState){
    state.authState = getState
  },

  FETCH_USER_SUCCESS (state, user) {
    state.user = user
  },

  FETCH_USER_FAILURE (state) {
    state.token = null
  },

  LOGOUT (state) {
    state.user = null
    state.token = null
    state.authState = null
  },

}

export const actions = {

  saveToken ({ commit, dispatch }, { token, remember }) {
    commit('SET_TOKEN', token)

    Cookies.set('token', token, { expires: remember ? 365 : 1 })
  },

  async fetchUser ({ commit }) {
    try {
      const { data } = await axios.get('user')

      commit('FETCH_USER_SUCCESS', data)
    } catch (e) {
      Cookies.remove('token')

      commit('FETCH_USER_FAILURE')
    }
  },

  async logout ({ commit }) {
    // try {
    //   await axios.get('logout')
    // } catch (e) { }

    Cookies.remove('token')

    commit('LOGOUT')
  },

  onSetAuthState ({ commit, dispatch }, { setAuthState }) {
    commit('SET_AUTH_STATE', setAuthState)
  },

}
