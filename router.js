import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Welcome = () => import('~/pages/index').then(m => m.default || m)
const Layout = () => import('~/pages/layout/index').then(m => m.default || m)


const routes = [
  { path: '/', name: 'welcome', component: Welcome },
  { path: '/layout', name: 'layout', component: Layout }
]

const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition
  }

  let position = {}

  if (to.matched.length < 2) {
    position = { x: 0, y: 0 }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    position = { x: 0, y: 0 }
  } if (to.hash) {
    position = { selector: to.hash }
  }

  return position
}

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history'
  })
}
