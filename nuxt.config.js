module.exports = {
  env: {
    apiUrl: 'http://127.0.0.1:3333/api/v1/',
    appName: 'Furious Social Network'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'frontend',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    bodyAttrs: {
      class: 'bodyTag'
    }
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Global Style
  */
  css: [
    '@/assets/styles.scss'
  ],
  /*
 ** Plugins
 */
  plugins: [
    {
      src: './plugins/tooltip'
    },
    {
      src: './plugins/axios',
      ssr: true
    },
    {
      src: './plugins/nuxt-client-init',
      ssr: true
    }
  ],
  /*
   ** Modules
   */
  modules: [
    '@nuxtjs/router'
  ],

  generate: {
    routes: [
      '/'
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev }) {
      if (isDev && process.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

